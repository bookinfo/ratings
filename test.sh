

STAGING_STATUS="FALSE"
SIT_ENVIRONMENT="FALSE"
IFS=',' read -r -a labels <<< "deploy::staging,deploy::sit::transaction,anoter-label"
for groups in "${labels[@]}"; do
    components=(`echo $groups | tr '::' ' '`)
    # for text in "${components[@]}"; do
    #     echo $text
    # done
    if [[ ${components[0]} == "deploy" ]]; then
        if [[ ${components[1]} == "sit" ]]; then
            #echo "Deploy Docker Image Tag ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME#*/}$CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA to ${element#*deploy::} Environment!"
            SIT_ENVIRONMENT = ${groups#*deploy::}
            STAGING_CHECK = "READY"
        fi
        if [[ ${components[1]} == "staging" ]]; then
            if [[ ${STAGING_CHECK} == "READY" ]]; then
                $STAGING_STATUS="TRUE"
            fi
        fi
    fi
done

if [[ $SIT_ENVIRONMENT != "FALSE" && STAGING_STATUS=="READY" ]]; then
    echo "Deploy to $SIT_ENVIRONMENT Environment!"
fi

if [[ $SIT_ENVIRONMENT != "FALSE" && STAGING_STATUS=="TRUE" ]]; then
    echo "Deploy to staging Environment!"
fi