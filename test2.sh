#IFS=',' read -r -a labels <<< "deploy::staging,deploy::sit::transaction,anoter-label"

# CI_MERGE_REQUEST_LABELS="deploy::staging,deploy::sit::transaction,anoter-label"

# if [[ $CI_MERGE_REQUEST_LABELS == *"deploy::sit::"* ]]; then
#     if [[ $CI_MERGE_REQUEST_LABELS == *"deploy::staging"* ]]; then
#         echo "Deploy to staging Environment!"
#     else
#         IFS=',' read -r -a labels <<< "deploy::staging,deploy::sit::transaction,anoter-label"
#         for groups in "${labels[@]}"; do
#             components=(`echo $groups | tr '::' ' '`)
#             if [[ ${components[0]} == "deploy" ]]; then
#                 if [[ ${components[1]} == "sit" ]]; then
#                     echo "Deploy to sit-${groups#*deploy::sit::} Environment!"
#                 fi
#             fi
#         done
#     fi
# fi

CREATE_VARIABLE_RESULT=$(curl --request POST --header "PRIVATE-TOKEN:BbgA7m7Y8jHp38cHMzSR" "https://gitlab.com/api/v4/projects/26245901/variables" --form "key=CURRENT_RELEASE_VERSION" --form "value=V1.0.0")
if [[ $CREATE_VARIABLE_RESULT == '{"message":{"key":["(CURRENT_RELEASE_VERSION) has already been taken"]}}' ]]; then
    curl --request PUT --header "PRIVATE-TOKEN:BbgA7m7Y8jHp38cHMzSR" "https://gitlab.com/api/v4/projects/26245901/variables/CURRENT_RELEASE_VERSION" --form "value=V1.0.2"
fi